package garmin.com.academyshop.networking;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import java.io.IOException;
import java.util.List;

import garmin.com.academyshop.MainActivity;
import garmin.com.academyshop.ProductsLoader;
import garmin.com.academyshop.db.AcademyShopDBManager;
import garmin.com.academyshop.model.Product;

/**
 * Created by attila on 15/05/2017.
 */

public class ProductFetchTask extends AsyncTask<Void, Void, List<Product>> {

    private static final String TAG = ProductFetchTask.class.getSimpleName();

    private Context mContext;

    public ProductFetchTask(Context context) {
        mContext = context;
    }

    @Override
    protected List<Product> doInBackground(Void... params) {

        RestApi restApi = new RestApi();
        try {
            return restApi.getData();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Product> products) {

        if(products != null) {
            AcademyShopDBManager.getInstance(mContext).clearDatabase();
            AcademyShopDBManager.getInstance(mContext).insertProducts(products);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(ProductsLoader.PRODUCTS_CHANGED));
        }

    }
}
