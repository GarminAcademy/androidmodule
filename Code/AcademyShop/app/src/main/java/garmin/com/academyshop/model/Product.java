package garmin.com.academyshop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by protiuc on 4/11/17.
 */

public class Product implements Parcelable {

    private int mId;
    private String mProductName;

    public Product(String productName){
        mProductName = productName;
    }

    public Product(int id, String productName){
        mId = id;
        mProductName = productName;
    }

    protected Product(Parcel in) {
        mId = in.readInt();
        mProductName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mProductName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }
}
