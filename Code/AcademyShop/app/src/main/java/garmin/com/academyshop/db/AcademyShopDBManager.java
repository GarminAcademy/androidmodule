package garmin.com.academyshop.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;

import garmin.com.academyshop.AcademyApplication;
import garmin.com.academyshop.ProductsLoader;
import garmin.com.academyshop.model.Product;

/**
 * Singleton class to manage database operations
 */
public class AcademyShopDBManager {

    /**
     * Singleton instance
     */
    private static AcademyShopDBManager mManager;

    private AcademyShopDBHelper mHelper;
    private SQLiteDatabase mDatabase;

    private Context mContext;

    private AcademyShopDBManager(Context context) {
        mHelper = new AcademyShopDBHelper(context);
        mContext = context;
    }

    public static AcademyShopDBManager getInstance(Context context) {
        if (mManager == null) {
            mManager = new AcademyShopDBManager(context);
        }
        return mManager;
    }

    public void openDatabase() {
        if (mDatabase == null || !mDatabase.isOpen()) {
            // Open database for writing
            mDatabase = mHelper.getWritableDatabase();
        }
    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public void clearDatabase() {
        // Assure database is open
        openDatabase();

        mDatabase.delete(AcademyShopContract.Products.TABLE_NAME, null, null);

        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).sendBroadcast(new Intent(ProductsLoader.PRODUCTS_CHANGED));
    }

    public void insertProducts(List<Product> products) {
        if (products != null && !products.isEmpty()) {

            // Assure database is open
            openDatabase();

            mDatabase.beginTransaction();
            try {
                for (int i = 0; i < products.size(); i++) {
                    // Create a ContentValues object for each Product to be inserted. Please note that ID shouldn't be added, because it is
                    // automatically added to inserted Product's row
                    ContentValues values = new ContentValues();
                    values.put(AcademyShopContract.Products.COLUMN_PRODUCT_NAME, products.get(i).getProductName());

                    // Insert each Product
                    mDatabase.insert(AcademyShopContract.Products.TABLE_NAME, null, values);
                }
                mDatabase.setTransactionSuccessful();
            } finally {
                mDatabase.endTransaction();
            }
        }
    }

    /**
     * @param ascending If true order Products by ascending, else by descending order
     */
    public List<Product> queryProducts(boolean ascending) {
        List<Product> products = new ArrayList<>();

        // Setup query arguments

        // Columns to return
        String[] projection = { AcademyShopContract.Products._ID, AcademyShopContract.Products.COLUMN_PRODUCT_NAME };

        // WHERE clause
        String selection = null;
        String[] selectionArgs = null;

        // GROUP BY clause
        String groupBy = null;

        // HAVING clause
        String having = null;

        // ORDER BY clause
        String orderBy = AcademyShopContract.Products.COLUMN_PRODUCT_NAME + (ascending ? " ASC" : " DESC");

        // Assure database is open
        openDatabase();

        // Execute query using arguments
        Cursor cursor = mDatabase.query(AcademyShopContract.Products.TABLE_NAME, projection, selection, selectionArgs, groupBy, having, orderBy);

        // Get returned Product rows from cursor
        if (cursor != null) {
            // Get column's index from cursor associated with columns
            int columnIndexProductId = cursor.getColumnIndex(AcademyShopContract.Products._ID);
            int columnIndexProductName = cursor.getColumnIndex(AcademyShopContract.Products.COLUMN_PRODUCT_NAME);

            // While there are items in the cursor get the Product data row-by-row
            while (cursor.moveToNext()) {
                // Get row data using column index of cursor
                int id = cursor.getInt(columnIndexProductId);
                String productName = cursor.getString(columnIndexProductName);

                // Create Product and add to list
                Product product = new Product(id, productName);
                products.add(product);
            }

            cursor.close();
        }

        return products;
    }
}
