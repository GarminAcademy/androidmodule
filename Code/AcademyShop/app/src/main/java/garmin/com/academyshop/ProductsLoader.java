package garmin.com.academyshop;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import garmin.com.academyshop.db.AcademyShopDBManager;
import garmin.com.academyshop.model.Product;

/**
 * Created by lungu on 5/4/2017.
 */

public class ProductsLoader extends AsyncTaskLoader<List<Product>> {

    public static final String PRODUCTS_CHANGED = "PRODUCTS_CHANGED";
    @Nullable
    private ProductChangeReceiver mBroadcastReceiver;

    @Nullable
    private List<Product> mProducts;

    public ProductsLoader(Context context) {
        super(context);
    }

    @Override
    public List<Product> loadInBackground() {
        mProducts = AcademyShopDBManager.getInstance(getContext()).queryProducts(true);
        return mProducts;
    }

    @Override
    public void deliverResult(List<Product> data) {
        if (isReset()) {
            releaseResources(data);
            return;
        }

        List<Product> oldProducts = mProducts;
        mProducts = data;

        if (isStarted()) {
            super.deliverResult(data);
        }

        if (oldProducts != null && oldProducts != data) {
            releaseResources(oldProducts);
        }
    }

    @Override
    protected void onStartLoading() {
        if (mProducts != null) {
            deliverResult(mProducts);
        }

        if (mBroadcastReceiver == null) {
            mBroadcastReceiver = new ProductChangeReceiver(this);
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter(PRODUCTS_CHANGED));
        }

        if (takeContentChanged() || mProducts == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();

        if (mProducts != null) {
            releaseResources(mProducts);
            mProducts = null;
        }

        if (mBroadcastReceiver != null){
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        }
    }

    @Override
    public void onCanceled(List<Product> data) {
        releaseResources(data);
    }

    private void releaseResources(List<Product> data) {
        // Release data
    }

    private static class ProductChangeReceiver extends BroadcastReceiver {

        private ProductsLoader mProductsLoader;

        public ProductChangeReceiver(ProductsLoader productsLoader) {
            mProductsLoader = productsLoader;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mProductsLoader.onContentChanged();
        }
    }
}
