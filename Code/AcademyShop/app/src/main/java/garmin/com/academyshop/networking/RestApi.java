package garmin.com.academyshop.networking;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import garmin.com.academyshop.model.Product;

import static android.content.ContentValues.TAG;

/**
 * Created by attila on 08/05/2017.
 */

public class RestApi {

    private static final String TAG = RestApi.class.getSimpleName();
    public static final String GET_DATA = "https://academy-90835.firebaseio.com/.json";
    private static final int MAX_STRING_LENGTH = 500;

    private static final String ADD_JSON = "{\"productName\" : \"NEW_PRODUCT_NAME\"}";

    public void addProduct(String... productParams) throws IOException {
        HttpsURLConnection connection = null;
        URL url = new URL(GET_DATA);

        connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);

        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(ADD_JSON.getBytes("UTF-8"));
        outputStream.close();

        connection.connect();

        int responseCode = connection.getResponseCode();

        Log.d(TAG, "addProduct response "+responseCode);

    }

    public List<Product> getData() throws IOException {
        List<Product> result = new ArrayList<>();

        InputStream inputStream = null;
        HttpsURLConnection connection = null;
        URL url = new URL(GET_DATA);

        connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setReadTimeout(3000);
        connection.setConnectTimeout(3000);
        connection.setDoInput(true);


        connection.connect();

        int responseCode = connection.getResponseCode();

        if(responseCode != HttpsURLConnection.HTTP_OK) {
            throw new IOException("Http error code "+responseCode);
        }

        inputStream = connection.getInputStream();

        if(inputStream != null) {
            result = readStream(inputStream);
        }

        return result;
    }

    private List<Product> readStream(InputStream inputStream) throws IOException {


        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));

        try {
            return readProductsArray(jsonReader);
        } finally {
            jsonReader.close();
        }

    }

    private List<Product> readProductsArray(JsonReader jsonReader) throws IOException {
        List<Product> products = new ArrayList<>();

        String productName = "";


        jsonReader.beginObject();
        while ((jsonReader.hasNext())) {

            String name = jsonReader.nextName();
            Log.d(TAG, "name "+name);
            jsonReader.beginObject();

            String prodNameHeader = jsonReader.nextName();
            if(prodNameHeader.equals("productName")) {
                productName = jsonReader.nextString();
                products.add(new Product((int) System.currentTimeMillis(), productName));
            } else {
                jsonReader.skipValue();
            }

            jsonReader.endObject();
        }

        jsonReader.endObject();

        return products;
    }

    private Product readProduct(JsonReader jsonReader) throws IOException {
        int id = 0;
        String productName = "";

        jsonReader.beginObject();
        while(jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            if(name.equals("id")) {
                id = jsonReader.nextInt();
            } else if(name.equals("productName")) {
                productName = jsonReader.nextString();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        return new Product(id, productName);

    }


    private String readStream(InputStream inputStream, int maxStringLength) throws IOException {
        String result = "";

        InputStreamReader streamReader = new InputStreamReader(inputStream, "UTF-8");
        char[] buffer = new char[maxStringLength];
        int numChars = 0;
        int readSize = 0;
        while (numChars < maxStringLength && readSize != -1) {
            numChars += readSize;
            readSize = streamReader.read(buffer, numChars, buffer.length-numChars);
        }

        if(numChars != -1) {
            numChars = Math.min(numChars, maxStringLength);
            result = new String(buffer, 0 , numChars);
        }


        return result;
    }

}
