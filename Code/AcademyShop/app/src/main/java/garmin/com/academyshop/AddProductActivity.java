package garmin.com.academyshop;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

import garmin.com.academyshop.networking.RestApi;

/**
 * Created by attila on 15/05/2017.
 */

public class AddProductActivity extends Activity {

    private EditText mEditText;

    private AddProductTask mAddTask;

    private RestApi mRestApi = new RestApi();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_layout);


        mEditText = (EditText) findViewById(R.id.editText);
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                    String product = mEditText.getText().toString();
                    if(!TextUtils.isEmpty(product)) {
                        mAddTask = new AddProductTask();
                        mAddTask.execute(product);
                    }

               }
           });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mAddTask != null) {
            mAddTask.cancel(true);
        }
    }

    private class AddProductTask extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... params) {
            try {
                mRestApi.addProduct(params);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}
