package garmin.com.academyshop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import garmin.com.academyshop.db.AcademyShopDBManager;
import garmin.com.academyshop.model.Product;
import garmin.com.academyshop.networking.NetworkUtils;
import garmin.com.academyshop.networking.ProductFetchTask;
import garmin.com.academyshop.networking.RestApi;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Product>>, NavigationView.OnNavigationItemSelectedListener, ProductAdapterListener {

    private static final int WRITE_STORAGE_PERMISSION_REQUEST = 0;
    private static final int PRODUCT_LIST_ID = 123;

    private InternetBroadcastReceiver mInternetBroadcastReceiver = new InternetBroadcastReceiver();
    private RecyclerView mRecyclerView;
    private ProductAdapter mAdapter;

    private List<Product> items;

    AcademyShopDBManager mDbManager;

    private ProductFetchTask mProductsTask;

    private boolean mConnected = false;

    ItemTouchHelper.SimpleCallback mSimpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            mAdapter.getItems().remove(viewHolder.getAdapterPosition());
            mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (navigationView.getHeaderCount() > 0) {
            View headerView = navigationView.getHeaderView(0);
            TextView accountTextView = (TextView) headerView.findViewById(R.id.account);
            String loginEmail = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_login_email), "");
            accountTextView.setText(loginEmail);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        // Get DB manager instance
        mDbManager = AcademyShopDBManager.getInstance(this);

        mAdapter = new ProductAdapter(new ArrayList<Product>(), this);
        mRecyclerView.setAdapter(mAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(mSimpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);

        getSupportLoaderManager().initLoader(PRODUCT_LIST_ID, null, this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mProductsTask = new ProductFetchTask(this);
        mProductsTask.execute();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mProductsTask.cancel(true);
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            mDbManager.closeDatabase();
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // Start settings activity when menu item is selected
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);

            return true;
        } else if (id == R.id.action_export_products) {
            exportProductsList();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            startActivity(new Intent(this, AddProductActivity.class));
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            AcademyShopDBManager.getInstance(this).clearDatabase();

        } else if (id == R.id.nav_logout) {
            // Clear the preferences (deletes all data from the SharedPreferences object it is called on)
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.clear();
            editor.apply();

            AcademyShopDBManager.getInstance(this).clearDatabase();

            // Finish this activity to return to login screen
            Intent loginScreenInNewTaskIntent = new Intent(this, LoginActivity.class);
            loginScreenInNewTaskIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginScreenInNewTaskIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        mConnected = checkConnectivityState(this);
        if (mInternetBroadcastReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
            registerReceiver(mInternetBroadcastReceiver, intentFilter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mInternetBroadcastReceiver != null) unregisterReceiver(mInternetBroadcastReceiver);
    }

    @Override
    public void onProductSelected(int position) {
        Toast.makeText(this, "Product selected " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Loader<List<Product>> onCreateLoader(int id, Bundle args) {
        return new ProductsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<Product>> loader, List<Product> data) {
        mAdapter.setItems(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Product>> loader) {
        mAdapter.setItems(new ArrayList<Product>());
    }

    public class InternetBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            mConnected = checkConnectivityState(context);

            if(mConnected) {
                Snackbar.make(findViewById(R.id.content_view), "Internet connection available", Snackbar.LENGTH_LONG).setAction("Action",
                        null).show();
            } else {
                Snackbar.make(findViewById(R.id.content_view), "No internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

        }
    }

    private boolean checkConnectivityState(Context context) {
        if(NetworkUtils.isConnected(context)) {
            if(NetworkUtils.isConnectedToWifiNetwork(context)) {
                return true;
            } else {

                if(NetworkUtils.isWifiOnlyNetworkPrefferenceSet(context)){
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    private void exportProductsList() {
        // Get shared preferences used in settings
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Get export option selected in settings
        String exportOption = sharedPreferences.getString(getString(R.string.key_storage_export), getString(R.string.export_value_external_storage));

        // If External Storage root directory is selected, so WRITE_EXTERNAL_STORAGE permission is required to succeed
        if (getString(R.string.export_value_external_storage).equals(exportOption) &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // If permission is not granted it should be requested
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE }, WRITE_STORAGE_PERMISSION_REQUEST);
        } else {
            saveProductsToExternalStorage(exportOption);
        }
    }

    private void saveProductsToExternalStorage(String exportOptionValue) {
        Intent startIntent = new Intent(this, SaveProductsService.class);
        startIntent.putExtra(SaveProductsService.KEY_EXPORT_OPTION, exportOptionValue);
        startService(startIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_STORAGE_PERMISSION_REQUEST:
                // Try to save the product. If permission was granted will succeed, else error toast will be shown
                saveProductsToExternalStorage(getString(R.string.export_value_external_storage));
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

}
