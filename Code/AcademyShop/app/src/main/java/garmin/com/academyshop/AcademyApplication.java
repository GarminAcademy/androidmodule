package garmin.com.academyshop;

import android.app.Application;

import com.facebook.stetho.Stetho;

import garmin.com.academyshop.networking.ProductFetchJobService;

/**
 * Created by lungu on 4/28/2017.
 */

public class AcademyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        ProductFetchJobService.startJob(this);
    }
}
