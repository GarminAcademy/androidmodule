package garmin.com.academyshop.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * SQLiteOpenHelper implementation for database creation
 */
public class AcademyShopDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "AcademyShop.db";

    /**
     * Version of the database. This should be incremented after each released change for onUpgrade to function.
     */
    private static final int DATABASE_VERSION = 1;

    public AcademyShopDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(AcademyShopContract.Products.CREATE_TABLE);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.beginTransaction();
        try {
            switch (oldVersion) {
                // Switch enters at case of old version and executes all declared SQL update scripts up to current version because of fallthrough
                // between cases. Version cases should be added in increasing order.
                case 2:
                    // Execute SQL here to update from version 1 to version 2
                    // Fallthrough
                case 3:
                    // Execute SQL here to update from version 2 to version 3
                    // Fallthrough
                case 4:
                    // Execute SQL here to update from version 3 to version 4
                    // Fallthrough
                default:
                    break;
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
