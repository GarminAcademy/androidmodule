package garmin.com.academyshop.db;

import android.provider.BaseColumns;

/**
 * Contract class for SQLite database
 */
public interface AcademyShopContract {

    /**
     * Products table contract class extending BaseColumns that contains _ID column declaration
     */
    interface Products extends BaseColumns {
        String TABLE_NAME = "academy_shop_products";
        String COLUMN_PRODUCT_NAME = "product_name";

        String CREATE_TABLE =
                        "CREATE TABLE " + AcademyShopContract.Products.TABLE_NAME + "(" + AcademyShopContract.Products._ID + " INTEGER PRIMARY KEY," +
                                        AcademyShopContract.Products.COLUMN_PRODUCT_NAME + " TEXT NOT NULL)";

        String DROP_TABLE = "DROP TABLE IF EXISTS '" + TABLE_NAME + "'";
    }
}
