package garmin.com.academyshop.model;

/**
 * Created by protiuc on 4/11/17.
 */

public class Product {
    private String mProductName;

    public Product(String productName){
        this.mProductName = productName;
    }


    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }
}
